package main;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.FileInputFormat;
import org.apache.hadoop.mapred.FileOutputFormat;
import org.apache.hadoop.mapred.JobClient;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reporter;
import org.apache.hadoop.mapred.RunningJob;
import org.apache.hadoop.mapred.TextInputFormat;
import org.apache.hadoop.mapred.TextOutputFormat;
import org.apache.hadoop.mapred.lib.ChainMapper;
import org.apache.hadoop.mapred.lib.ChainReducer;

public class Runner {

  /**
   * @param args
   */
  public static void main(String[] args) throws Exception
  {
        /*JobConf conf = new JobConf(Runner.class);
        conf.setJobName("ip-count");
        
        conf.setMapperClass(IpMapper.class);
        //LongWritable fileOffset, Text lineContents, OutputCollector<Text, IntWritable> output, Reporter reporter
        JobConf confA = new JobConf(false);
        ChainMapper.addMapper(conf, IpMapper.class, IntWritable.class, Text.class,IntWritable.class, Text.class, true, confA);
        conf.setMapOutputKeyClass(Text.class);
        conf.setMapOutputValueClass(IntWritable.class);
        conf.setCombinerClass(IpReducer.class);
        conf.setReducerClass(IpReducer.class);
        //conf.setMapperClass(AnotherMapper.class);
        
        // take the input and output from the command line
        FileInputFormat.setInputPaths(conf, new Path(args[0]));
        FileOutputFormat.setOutputPath(conf, new Path(args[1]));

        JobClient.runJob(conf);*/
        
        
        JobConf conf = new JobConf(new Configuration(), IpMapper.class);
        conf.setJobName("wordcount");

        FileInputFormat.setInputPaths(conf, args[0]);
        FileOutputFormat.setOutputPath(conf, new Path(args[1]));
        conf.setMapOutputKeyClass(Text.class);
        conf.setMapOutputValueClass(IntWritable.class);
        conf.setInputFormat(TextInputFormat.class);
        conf.setOutputFormat(TextOutputFormat.class);
        
        conf.setNumMapTasks(2);
        conf.setNumReduceTasks(2);
        conf.setMapperClass(IpMapper.class);
        FileInputFormat.setInputPaths(conf, new Path(args[0]));
        FileOutputFormat.setOutputPath(conf, new Path(args[1]));
       
        ChainReducer chainReducer = new ChainReducer();
        
        JobConf reduceConf = new JobConf(false);
        chainReducer.setReducer(conf, IpReducer.class, Text.class, IntWritable.class,Text.class, IntWritable.class, true, reduceConf);
        
        JobConf mapBConf = new JobConf(false);
        chainReducer.addMapper(conf, AnotherMapper.class, Text.class, IntWritable.class,Text.class, IP.class, true, mapBConf);
        
        /*JobConf mapCConf = new JobConf(false);
        chainReducer.addMapper(conf, FinalMapper.class, Text.class, IntWritable.class,Text.class, IntWritable.class, true, mapBConf);
       */ 
        JobConf mapDConf = new JobConf(false);
       // chainReducer.setReducer(conf, FinalReducer.class, Text.class, IP.class,Text.class, IntWritable.class, true, mapDConf);
        
        conf.setReducerClass(FinalReducer.class);
        /*conf.setMapOutputKeyClass(Text.class);
        conf.setMapOutputValueClass(IP.class);*/
        //conf.setOutputKeyClass(theClass);
        //chainReducer.addMapper(conf, FinalMapper.class, Text.class, IntWritable.class,Text.class, IntWritable.class, true, mapBConf);
        //Text, IP, Text, IntWritable
        chainReducer.configure(conf);
        
        JobClient jc = new JobClient(conf);
        RunningJob job = jc.submitJob(conf);
        JobClient.runJob(conf);


	}

}
