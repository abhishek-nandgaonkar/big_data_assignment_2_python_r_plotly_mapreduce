package main;

import java.io.IOException;
import java.util.Map.Entry;
import java.util.NavigableMap;
import java.util.TreeMap;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.Mapper;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reporter;

public class FinalMapper extends MapReduceBase 
implements Mapper<Text, IntWritable , Text, IntWritable>
{
	private static TreeMap<Integer, String> treeMap = new TreeMap<Integer, String>();
	
@Override
public void map(Text ip, IntWritable count, OutputCollector<Text, IntWritable> output, Reporter reporter)
		throws IOException {
	// TODO Auto-generated method stub
	System.out.println("printing from final mapper");
	treeMap = AnotherMapper.treeMap;
	//treeMap.put(count.get(), ip.toString());

	 NavigableMap<Integer, String> nmap=treeMap.descendingMap();
	 for(Entry<Integer, String> entry: nmap.entrySet()) // iterate key/value entries.

	{
		//System.out.print(entry.getKey() + "\t");
		//System.out.println(entry.getValue());
		
	output.collect(new Text(entry.getValue()),new IntWritable(entry.getKey()));
		//Log.info(entry.getKey());
		//Log.info(entry.getValue());
	}
	
	

}
}
