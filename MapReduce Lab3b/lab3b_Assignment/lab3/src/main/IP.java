package main;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.Writable;

class IP implements Comparable<IP>, Writable
{
	private String IP;
	private Integer count;
	
	@Override
	public int compareTo(IP ipValue) {
		int counter = ((IP)ipValue).getCount();
		return this.count-counter;
	}
	
	public String getIP() {
		return IP;
	}

	public void setIP(String iP) {
		IP = iP;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	IP(String IP, Integer count)
	{
		super();
		this.IP = IP;
		this.count = count;
	}
	
	@Override
	public void write(DataOutput out) throws IOException {
		out.writeBytes(IP);
		out.writeInt(count);
	}
	@Override
	public void readFields(DataInput in) throws IOException {
		IP = in.readLine();
		count = in.readInt();
	}

	

	
}
