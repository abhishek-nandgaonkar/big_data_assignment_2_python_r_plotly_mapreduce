package main;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reducer;
import org.apache.hadoop.mapred.Reporter;

public class FinalReducer extends MapReduceBase implements
Reducer<Text, IP, Text, IntWritable> {

ArrayList<IP> ipList = new ArrayList<IP>();
public void reduce(Text ip, Iterator<IP> values,
	OutputCollector<Text, IntWritable> output, Reporter reporter)
	throws IOException {
	
IP ipInfo = null;
while(values.hasNext()){
	//String ipValue  = values.next().getIp();
	ipList.add(values.next());
	
}

Collections.sort(ipList);

for(int i=0; i< ipList.size(); i++){
	output.collect(new Text(ipList.get(i).getIP()), new IntWritable(ipList.get(i).getCount()));
}
	
}
}
