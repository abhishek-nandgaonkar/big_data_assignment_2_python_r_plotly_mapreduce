## exercise: select 2x2 subsection from the "bottom left" of matrix mm

## [your code here]

mm[c(3,4),c(1,2)]

============================================================================

## EXERCISE
## find max and min tuition ("Outstate") grouped by private/public
## school, in dataset 'df' and 'college'

## DF:
##
##   Private   max  min
## 1      No  9766 3946
## 2     Yes 19240 6398

sol1 <- group_by(df,Private)
sol1 <- summarize(sol1, minTuition=min(Outstate), maxTuition=max(Outstate))
sol1

## college:
##
##   Private   max  min
## 1      No 15732 2580
## 2     Yes 21700 2340

sol1 <- group_by(college,Private)
sol1 <- summarize(sol1, minTuition=min(Outstate), maxTuition=max(Outstate))
sol1

==============================================================================

## EXERCISE
## obtain this data view from "df":

##                              X Grad.Rate
## 1     James Madison University        98
## 2       Incarnate Word College        95
## 3     Johns Hopkins University        90
## 4      John Carroll University        89
## 5               Kenyon College        88
## 6               King's College        87
## 7          La Salle University        84
## 8 Illinois Wesleyan University        83
## 9              Juniata College        80


## [your code here]
x <- filter(df,Grad.Rate>=80)
x
select(df,College.name,x)