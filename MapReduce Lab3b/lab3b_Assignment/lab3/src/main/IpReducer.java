package main;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reducer;
import org.apache.hadoop.mapred.Reporter;
import org.mortbay.log.Log;

/**
 * Counts all of the hits for an ip. Outputs all ip's
 */
/*class IP implements Comparable<IP>
{
	private String IP;
	private Integer count;
	
	@Override
	public int compareTo(IP ipValue) {
		int counter = ((IP)ipValue).getCount();
		return this.count-counter;
	}
	
	public String getIP() {
		return IP;
	}

	public void setIP(String iP) {
		IP = iP;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	IP(String IP, Integer count)
	{
		super();
		this.IP = IP;
		this.count = count;
	}
	
	
}*/

public class IpReducer extends MapReduceBase implements
		Reducer<Text, IntWritable, Text, IntWritable> {
	
	 //static ArrayList<IP> ipList = new ArrayList<IP>();
	public static TreeMap<Integer, Text> treeMap = new TreeMap<Integer, Text>();
	
	public void reduce(Text ip, Iterator<IntWritable> counts,
			OutputCollector<Text, IntWritable> output, Reporter reporter)
			throws IOException {
		System.out.println("printing from ip reducer");
		
	//	System.out.println("IP LIST: " +ipList);
		//IP ips = null;
		
		
		int totalCount = 0;

		// loop over the count and tally it up
		while (counts.hasNext()) {
			IntWritable count = counts.next();
			totalCount += count.get();
		}
		if (totalCount > 100) {
			//System.out.println("putting in the treemap");
			//treeMap.put(totalCount, ip);
	//		ips = new IP(ip.toString(), totalCount);
//			ipList.add(ips);
			output.collect(ip, new IntWritable(totalCount));
		}
		
		/*for(IP ipIterate: ipList)
		{
			System.out.print(ipIterate.getIP() + "\t");
			System.out.println(ipIterate.getCount());
			//System.out.println(ipList);
		}*/
		
		
		for(Entry<Integer, Text> entry: treeMap.entrySet())
		{
			//System.out.print(entry.getKey() + "\t");
			//System.out.println(entry.getValue());
			
			//output.collect(entry.getValue(), new IntWritable(entry.getKey()));
			//Log.info(entry.getKey());
			//Log.info(entry.getValue());
		}
		//Collections.sort(ipList);
		/*for(IP ipIterate: ipList)
		{
			//System.out.print(ipIterate.getIP() + "\t");
			//System.out.println(ipIterate.getCount());
			//System.out.println(ipList);
		}*/
		

	}

}
